Quickstart
==========
This quickstart example shows how to easily build 3D representations of biological structures with PyUUL.

Importing libraries 
^^^^^^^^^^^^^^^^^^^

You can use the following code to import all the necessary libraries used in this example:

.. code-block:: python

  from pyuul import VolumeMaker # the main PyUUL module
  from pyuul import utils # the PyUUL utility module
  import time,os,urllib # some standard python modules we are going to use

Collecting the data
^^^^^^^^^^^^^^^^^^^

We now need some protein structures to test. You can download them from the PDB website (https://www.rcsb.org/). In this case, we will get them using urllib, a standard and preinstalled library of python. This will allow a painless data collection with a simple copy-paste of the code. Lets fetch a couple of structures:

.. code-block:: python

   os.mkdir('exampleStructures')
   urllib.request.urlretrieve('http://files.rcsb.org/download/101M.pdb', 'exampleStructures/101m.pdb')
   urllib.request.urlretrieve('http://files.rcsb.org/download/5BMZ.pdb', 'exampleStructures/5bmz.pdb')
   urllib.request.urlretrieve('http://files.rcsb.org/download/5BOX.pdb', 'exampleStructures/5box.pdb')

Parsing the structures
^^^^^^^^^^^^^^^^^^^^^^

Next step is to parse the information of the structures. In order to do so, we can use the utility module of PyUUL:

.. code-block:: python

   coords, atname = utils.parsePDB("exampleStructures/") # get coordinates and atom names 
   atoms_channel = utils.atomlistToChannels(atname) # calculates the corresponding channel of each atom
   radius = utils.atomlistToRadius(atname) # calculates the radius of each atom

Volumetric representations
^^^^^^^^^^^^^^^^^^^^^^^^^^
Now we have everything we need to get the volumetric representations. We can build the main PyUUL objects on CPU with:

.. code-block:: python

   device = "cpu" # runs the volumes on CPU
   VoxelsObject = VolumeMaker.Voxels(device=device,sparse=True)
   PointCloudSurfaceObject = VolumeMaker.PointCloudVolume(device=device)
   PointCloudVolumeObject = VolumeMaker.PointCloudSurface(device=device)

   
or on GPU with:

.. code-block:: python

   device = "cuda" # runs the volumes on GPU (you need a cuda-compatible GPU computer for this)
   VoxelsObject = VolumeMaker.Voxels(device=device,sparse=True)
   PointCloudSurfaceObject = VolumeMaker.PointCloudVolume(device=device)
   PointCloudVolumeObject = VolumeMaker.PointCloudSurface(device=device)

Next, we move everything to the right device:

.. code-block:: python

   coords = coords.to(device)
   radius = radius.to(device)
   atoms_channel = atoms_channel.to(device)

Finally, we obtain the volumetric representation of the proteins with:

.. code-block:: python

   SurfacePoitCloud = PointCloudSurfaceObject(coords, radius)
   VolumePoitCloud = PointCloudVolumeObject(coords, radius)
   VoxelRepresentation = VoxelsObject(coords, radius, atoms_channel)
