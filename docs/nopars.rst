PyUUL without using the parsers
===============================
PyUUL allows the user to manually define the structure, bypassing the parsers. This might be useful in order to deal with exotic atoms that are not supported by the parsers, but also in order to use PyUUL as an intermediate step in an end-to-end neural network.

As usual, we first need to import PyUUL modules:

.. code-block:: python

  from pyuul import VolumeMaker # the main PyUUL module
  from pyuul import utils # the PyUUL utility module
  import torch


We then generate the main PyUUL python objects:

.. code-block:: python

   device = "cpu"
   VoxelsObject = VolumeMaker.Voxels(device=device,sparse=True)
   PointCloudVolumeObject = VolumeMaker.PointCloudVolume(device=device)
   PointCloudSurfaceObject = VolumeMaker.PointCloudSurface(device=device)

Point clouds object only require the coordinates and radius tensors.

.. code-block:: python

   device = "cpu"
   coordinates = torch.rand((5,10,3),device=device) #5 molecules of 10 atoms each
   radius = torch.rand((5,10),device=device) #radius of each atom

   SurfacePoitCloud = PointCloudSurfaceObject(coordinates, radius)
   VolumePoitCloud = PointCloudVolumeObject(coordinates, radius)

For voxelized representation, we also need channels

.. code-block:: python

   channels = torch.randint(size=(5,10),low=0,high=5,device=device)
   voxelRepresentation = VoxelsObject(coordinates, radius, channels)

Volumetric representations are differentiable, so autograd gradient of coordinates is not killed by this operation:

.. code-block:: python

   coordinates.requires_grad = True
   channels = torch.randint(size=(5,10),low=0,high=5,device=device)
   voxelRepresentation = VoxelsObject(coordinates, radius, channels).to_dense() #sparse backward is not implemented in pytorch

   toy_loss = voxelRepresentation.sum()
   toy_loss.backward() # the gradient is propagated to coordinates

