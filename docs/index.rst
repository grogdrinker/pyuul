.. PyUUL documentation master file, created by
   sphinx-quickstart on Tue May 25 16:35:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyUUL's documentation!
=================================

pyUUL is a Python library designed to process 3D-structures of macromolecules, such as PDBs, translating them into fully differentiable data structures. These data structures can be used as input for any modern neural network architecture. Currently, the user can choose between three different types of data representation: voxel-based, surface point cloud and volumetric point cloud.

.. toctree::
   :maxdepth: 2
   
   install
   introduction
   quickstart
   smallMol
   nopars
   examples/index
   contacts
   modules
      



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
