Installation guide
==================

How do I set it up?
^^^^^^^^^^^^^^^^^^^

You can install PyUUL either from the bitbucket (https://bitbucket.org/grogdrinker/pyuul/) repository, from a pypi package (https://pypi.org/project/pyuul/) or from a conda package (https://anaconda.org/anaconda/pyuul).


Installing PyUUL with pip:
^^^^^^^^^^^^^^^^^^^^^^^^^^
If you want to install PyUUL via Pypi, just open a terminal and type:

``pip install pyuul``

and all the required dependencies will be installed automatically. Congrats, you are ready to rock!


Installing PyUUL with Anaconda
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PyUUL can be installed simply via conda by typing in the terminal:

``conda install -c grogdrinker pyuul``

again, all the required dependencies will be installed automatically.

Installing PyUUL from source:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
If you want to install PyUUL from this repository, you need to install the dependencies first.

Install ``pytorch >= 1.0`` with the command: ``conda install pytorch -c pytorch`` or refer to ``pytorch`` website https://pytorch.org.

Install the remaining requirements with the command: ``conda install scipy numpy scikit-learn``.

You can remove this environment at any time by typing: ``conda remove -n pyuul --all``.

Finally, you can clone the repository with the following command:

``git clone https://bitbucket.org/grogdrinker/pyuul/``
