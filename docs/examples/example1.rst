Alpha Helices identification
============================

This tutorial shows how to build a neural network that takes voxelized proteins as input. The tutorial replicates the example done in the paper.

.. raw:: html
  :file: alphaHelix.html



