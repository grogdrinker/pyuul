PyUUL with Small Molecules
==========================
This guide shows how handle small molecules in PyUUL. For this purpose we will use the parser for SDF files provided by PyUUL.

Importing libraries
^^^^^^^^^^^^^^^^^^^

You can use the following code to import all the necessary libraries used in this example:

.. code-block:: python

  from pyuul import VolumeMaker # the main PyUUL module
  from pyuul import utils # the PyUUL utility module
  import os,urllib # some standard python modules we are going to use

Using the parser for small molecules (SDF files)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We now need some small molecule structures to test. You can download them from the PDB website (https://www.rcsb.org/). As in the quickstart guide, we will get them using urllib, a standard and preinstalled library of python. This will allow a painless data collection with a simple copy-paste of the code. Lets fetch a couple of structures:

.. code-block:: python

   os.mkdir('exampleStructures')
   urllib.request.urlretrieve('https://files.rcsb.org/ligands/view/GTP_ideal.sdf', 'exampleStructures/gtp.sdf')
   urllib.request.urlretrieve('https://files.rcsb.org/ligands/view/CFF_ideal.sdf', 'exampleStructures/cff.sdf')
   urllib.request.urlretrieve('https://files.rcsb.org/ligands/view/CLR_ideal.sdf', 'exampleStructures/clr.sdf')

Next step is to parse the information of the structures. In order to do so, we can use the utility module of PyUUL. The syntax is the same of when dealing with PDBs, but in this case we use the function parseSDF:

.. code-block:: python

   coords, atname = utils.parseSDF("exampleStructures/") # get coordinates and atom names
   atoms_channel = utils.atomlistToChannels(atname) # calculates the corresponding channel of each atom
   radius = utils.atomlistToRadius(atname) # calculates the radius of each atom

From now on, everything works the same way of PDBs. We can build the main PyUUL objects on CPU with:

.. code-block:: python

   device = "cpu" # runs the volumes on CPU. use "cuda" for GPU. Pytorch needs to be installed with cuda support in this case.
   VoxelsObject = VolumeMaker.Voxels(device=device,sparse=True)
   PointCloudSurfaceObject = VolumeMaker.PointCloudVolume(device=device)
   PointCloudVolumeObject = VolumeMaker.PointCloudSurface(device=device)


We move everything to the right device:

.. code-block:: python

   coords = coords.to(device)
   radius = radius.to(device)
   atoms_channel = atoms_channel.to(device)

Finally, we obtain the volumetric representations with:

.. code-block:: python

   SurfacePoitCloud = PointCloudSurfaceObject(coords, radius)
   VolumePoitCloud = PointCloudVolumeObject(coords, radius)
   VoxelRepresentation = VoxelsObject(coords, radius, atoms_channel)