#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

import torch
class Nuovomod(torch.nn.Module):

    def __init__(self, inchannels, numberofFeatures=2):
        super(Nuovomod, self).__init__()
        dilatation = 4
        stride = 2
        kernel_size = 5
        outchannel = 5
        final_outchannels = 30
        padding = 1
        self.softmax = torch.nn.Softmax(2)
        dropoutProb = 0.0

        hiddenLayer = 40
        self.final_net = torch.nn.Sequential(torch.nn.Linear(final_outchannels, hiddenLayer),
                                             torch.nn.LayerNorm(hiddenLayer),
                                             torch.nn.Dropout(dropoutProb),
                                             torch.nn.Tanh(),
                                             torch.nn.Linear(hiddenLayer, hiddenLayer),
                                             torch.nn.LayerNorm(hiddenLayer),
                                             torch.nn.Dropout(dropoutProb),
                                             torch.nn.Tanh(),
                                             torch.nn.Linear(hiddenLayer, numberofFeatures),
                                             torch.nn.Tanh()
                                             )

        self.predict = torch.nn.Sequential(
            torch.nn.Conv3d(inchannels, outchannel, kernel_size=kernel_size, padding=(kernel_size - 1) // 2),
            # torch.nn.BatchNorm3d(outchannel),
            torch.nn.Dropout(dropoutProb),
            torch.nn.InstanceNorm3d(outchannel, affine=True),
            torch.nn.Tanh(),
            torch.nn.Conv3d(outchannel, final_outchannels, kernel_size=kernel_size, padding=(kernel_size - 1) // 2),
            torch.nn.Dropout(dropoutProb),
            # torch.nn.BatchNorm3d(outchannel),
            # torch.nn.InstanceNorm3d(outchannel,affine=True),
            # torch.nn.Tanh(),
            # torch.nn.Conv3d(outchannel, outchannel, kernel_size=kernel_size, padding=(kernel_size - 1) // 2),
            # torch.nn.BatchNorm3d(outchannel),
            torch.nn.InstanceNorm3d(final_outchannels, affine=True),
            torch.nn.Tanh(),
            # MaskedConv3d(d // 2, d, kernel_size=kernel_size),
            # maskedBatchNorm3d(d),
            # maskedRelu(),
            # ResBlock(d, d, bn=True),
            # maskedBatchNorm3d(d),
            # ResBlock(d, d, bn=True),
        )

        self.attention = torch.nn.Sequential(
            torch.nn.Conv3d(inchannels, outchannel, kernel_size=kernel_size, padding=(kernel_size - 1) // 2),
            # torch.nn.BatchNorm3d(outchannel),
            # torch.nn.InstanceNorm3d(outchannel,affine=True),
            # torch.nn.Tanh(),
            # torch.nn.Conv3d(outchannel, outchannel, kernel_size=kernel_size, padding=(kernel_size - 1) // 2),
            # torch.nn.BatchNorm3d(outchannel),
            torch.nn.Dropout(dropoutProb),
            torch.nn.InstanceNorm3d(outchannel, affine=True),
            torch.nn.Tanh(),
            torch.nn.Conv3d(outchannel, final_outchannels, kernel_size=kernel_size, padding=(kernel_size - 1) // 2),
            torch.nn.Dropout(dropoutProb),
            # torch.nn.BatchNorm3d(outchannel),
            torch.nn.InstanceNorm3d(final_outchannels, affine=True),
            torch.nn.Tanh(),
        )
        kernel1 = 5
        padding = 2
        stride = 1
        kernel2 = 5
        self.channels = 2

        outchannelLocaliz = 5

        self.localizationPred = torch.nn.Sequential(
            torch.nn.Conv3d(inchannels, outchannelLocaliz, kernel_size=kernel1, stride=stride, padding=padding),
            torch.nn.MaxPool3d(2, stride=2),
            torch.nn.InstanceNorm3d(outchannelLocaliz, affine=True),
            torch.nn.Tanh(),
            # torch.nn.Conv3d(8, outchannelLocaliz, kernel_size=kernel2,stride=stride,padding=padding),
            # torch.nn.MaxPool3d(2, stride=2),
            # torch.nn.InstanceNorm3d(outchannelLocaliz,affine=True),
            # torch.nn.Tanh()
        )

        self.localizationAtte = torch.nn.Sequential(
            torch.nn.Conv3d(inchannels, outchannelLocaliz, kernel_size=kernel1, stride=stride, padding=padding),
            torch.nn.MaxPool3d(2, stride=2),
            torch.nn.InstanceNorm3d(outchannelLocaliz, affine=True),
            torch.nn.Tanh(),
            # torch.nn.Conv3d(8, outchannelLocaliz, kernel_size=kernel2,stride=stride,padding=padding),
            # torch.nn.MaxPool3d(2, stride=2),
            # torch.nn.InstanceNorm3d(outchannelLocaliz,affine=True),
            # torch.nn.Tanh()
        )

        self.fc_locSA = torch.nn.Sequential(
            torch.nn.Linear(outchannelLocaliz, 32),
            torch.nn.ReLU(True),
            torch.nn.Linear(32, 3 * 4)
        )
        self.fc_locSA[2].weight.data.zero_()

        # self.fc_loc[2].bias.data.copy_(torch.tensor([1, 0, 0, 0, 1, 0], dtype=torch.float))

    def stnSA(self, x):
        xsp = self.localizationPred(x)
        xsp = xsp.view(xsp.shape[0], xsp.shape[1], -1)

        xsa = self.localizationAtte(x)
        xsa = self.softmax(xsa.view(xsa.shape[0], xsa.shape[1], -1))
        fin = (xsp * xsa).sum(-1)

        theta = self.fc_locSA(fin)
        theta = theta.view(-1, 3, 4)

        grid = F.affine_grid(theta, x.size(), align_corners=False)
        x = F.grid_sample(x, grid, align_corners=False)

        return x

    def forward(self, x):
        batch_size = x[0].shape[0]

        vals = x[0]
        # mask = x[1]
        orientedx = self.stnSA(vals)

        orientedx = orientedx
        del x, vals

        pred = self.predict(orientedx)
        channels = pred.shape[1]
        # pred = pred.masked_fill_(mask,0.0).view(batch_size,channels,-1)
        pred = pred.view(batch_size, channels, -1)

        atte = self.attention(orientedx)
        # atte = self.softmax(atte.masked_fill_(mask, 0.0).view(batch_size,channels,-1))
        atte = self.softmax(atte.view(batch_size, channels, -1))
        del orientedx
        fin = (atte * pred).sum(-1)
        return self.final_net(fin)
